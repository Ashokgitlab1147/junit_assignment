package com.innominds.junit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class Q24_01_ManagerTest {

	Q24_01_Manager obj = new Q24_01_Manager(20000,15000,2000,800,200);
	@Test
	void testNetSalary() {
		
		double actualResult = obj.netSalary();
		double expectedResult = 32000;
		
		assertEquals(expectedResult , actualResult);
		
	}
	@Test
	void testDisplaySalaryInfo() {
		//boolean actualResult = obj.displaySalaryInfo();
		assertTrue(obj.displaySalaryInfo());
	}

}
