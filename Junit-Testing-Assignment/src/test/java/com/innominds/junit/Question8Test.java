package com.innominds.junit;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class Question8Test {
	Question8 obj = new Question8();
	@Test
	@DisplayName(">>>Testing: printEvenNumbers()")
	void testPrintEvenNumbers() {
		ArrayList<Integer> list = new ArrayList<>(Arrays.asList(0,1,2,3,4,5,9,10));
		ArrayList<Integer> actualResult = obj.printEvenNumbers(list);
		ArrayList<Integer> expectedResult =  new ArrayList<Integer>(Arrays.asList(10,4,2,0));
		Collections.sort(expectedResult);
		
		assertEquals(expectedResult,actualResult);
	}

}
