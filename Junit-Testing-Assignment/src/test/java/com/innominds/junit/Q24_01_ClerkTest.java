package com.innominds.junit;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Q24_01_ClerkTest {

	Q24_01_Manager obj = new Q24_01_Manager(10000,5000,1000,500,200);
	@Test
	void testNetSalary() {
		
		double actualResult = obj.netSalary();
		double expectedResult = 13300;
		
		assertEquals(expectedResult , actualResult);
		
	}
	@Test
	void testDisplaySalaryInfo() {
		//boolean actualResult = obj.displaySalaryInfo();
		assertTrue(obj.displaySalaryInfo());
	}

}
