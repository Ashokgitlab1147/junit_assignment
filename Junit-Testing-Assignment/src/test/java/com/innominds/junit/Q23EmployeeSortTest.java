package com.innominds.junit;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

class Q23EmployeeSortTest {
	
	@Test
	void testSortEmployees() {
		Q23EmployeeSort obj = new Q23EmployeeSort();
		List<Q23Employee> empList = new ArrayList<>();
		empList.add(new Q23Employee(1,"Ram","HR"));
		empList.add(new Q23Employee(3,"Balu","Associate Engineer"));
		empList.add(new Q23Employee(2,"John","Senior Software Engineer"));
		empList.add(new Q23Employee(4,"Abhi Ram","Analyst"));
		
		List<String> actualResult = obj.sortEmployees(empList);
		List<String> expectedResult = Arrays.asList("Abhi Ram", "Balu", "John", "Ram");
		
		assertEquals(expectedResult , actualResult);
	}

}
