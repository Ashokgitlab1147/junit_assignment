package com.innominds.junit;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class Q7DateDifferenceTest {

	@Test
	@DisplayName("Difference between two dates")
	void testDiffBetweenTwoDates() {
		Q7DateDifference obj = new Q7DateDifference();
		LocalDate d1= LocalDate.of(2000,11,8);
		LocalDate d2= LocalDate.of(2022, 9, 8);
		String actualResult = obj.diffBetweenDates(d1, d2);
		String expectedResult = "21 years,10 months,0 days";
		assertEquals(expectedResult , actualResult);
	}

}
