package com.innominds.junit.jdbc;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.sql.SQLException;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class JdbcConnectionTest {

	private static JdbcConnection obj;
	static boolean status;
	@BeforeAll
	static void init() throws SQLException {
		obj = new JdbcConnection();
		status = obj.createConnection();
	}
	
	@Test
	void testJdbcConnection() throws SQLException {
		
		assertTrue(JdbcConnectionTest.status);
	}

	@Test
	void testTablesCount() throws SQLException {
		
		String query = "SELECT COUNT(*) FROM information_schema.tables as Tables_Count WHERE table_schema = 'world';";
		int actualResult = obj.countTablesInDatabase(query);
		int expectedResult = 6;
		
		assertEquals(expectedResult , actualResult);
		
	}
}
