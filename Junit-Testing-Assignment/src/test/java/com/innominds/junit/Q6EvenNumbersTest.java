package com.innominds.junit;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class Q6EvenNumbersTest {

	private static Q6EvenNumbers obj;
	@BeforeAll
	static void init() {
		obj = new Q6EvenNumbers();
	}
	@Test
	void testSaveEvenNumbers() {
		ArrayList<Integer> actualResult = obj.saveEvenNumbers(10);
		ArrayList<Integer> expectedResult = new ArrayList<>(Arrays.asList(2,4,6,8,10));
		
		assertEquals(expectedResult , actualResult);
	}
	@Test
	void testPrintEvenNUmbers() {
		ArrayList<Integer> evenNumbers = obj.saveEvenNumbers(10);
		ArrayList<Integer> actualResult = obj.printEvenNumbers();
		ArrayList<Integer> expectedResult = new ArrayList<>(Arrays.asList(4,8,12,16,20));
		
		assertEquals(expectedResult , actualResult);
	}

}
