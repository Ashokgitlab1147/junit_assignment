package com.innominds.junit;


import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class Q4DaemonThreadTest {

	Q4DaemonThread thread = new Q4DaemonThread("T1");
	Daemon obj = new Daemon();
	@Test
	void testIsDaemonThread() {
		thread.setDaemon(true);
		assertTrue(obj.checkDaemonThread(thread));
	}
	@Test
	void testDaemonThreadException() {
		assertThrows(IllegalThreadStateException.class , ()->obj.daemonThreadException(thread));
	}
}
