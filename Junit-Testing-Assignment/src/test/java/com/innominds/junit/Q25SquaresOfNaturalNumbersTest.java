package com.innominds.junit;

import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class Q25SquaresOfNaturalNumbersTest {

	Q25SquaresOfNaturalNumbers obj = new Q25SquaresOfNaturalNumbers();
	@Test
	void test() throws OverflowException {
		//List<Integer> actualResult = obj.printSquares(10);
		
		//List<Integer> expectedResult = Arrays.asList(1,4,9,16,25,36,49,64,81,100);
		
		assertTrue(obj.printSquares(10));
	}
	@Test
	void testOverflowException() {
		assertThrows(OverflowException.class,()->obj.printSquares(11));
	}

}
