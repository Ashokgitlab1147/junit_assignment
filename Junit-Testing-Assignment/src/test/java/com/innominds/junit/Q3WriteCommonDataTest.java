package com.innominds.junit;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;

import org.junit.jupiter.api.Test;

class Q3WriteCommonDataTest {

	Q3WriteCommonData obj = new Q3WriteCommonData();
	@Test
	void testIsCommonDataWritten() {
		String str="Hi Folks! Welcome to Unit Testing Class\nToday we are going to discuss Junit framework";
		try {
			obj.writeCommonData(str);
		} catch (IOException e) {
			e.printStackTrace();
		}
		assertTrue(obj.isCommonDataWritten());
	}

}
