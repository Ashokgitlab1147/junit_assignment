package com.innominds.junit;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Q11VehiclesDirectionTest {
	Q11VehiclesDirection obj = new Q11VehiclesDirection();
	@Test
	void testCheckDirection() throws DirectionException {
		int actualResult = obj.checkDirection("forward", "forward");
		int expectedResult = 1;
		
		assertEquals(expectedResult , actualResult);
		
	}
	
	@Test
	void testCheckDirectionException() throws DirectionException {
		assertThrows(DirectionException.class , ()->obj.checkDirection("forward", "opposite"));
		
	}

}
