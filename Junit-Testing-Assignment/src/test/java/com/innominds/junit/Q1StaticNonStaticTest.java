package com.innominds.junit;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class Q1StaticNonStaticTest {

	@Test
	@DisplayName("Static Variable Counting")
	void staticCountTest() {
		Q1StaticNonStatic obj1 = new Q1StaticNonStatic();
		Q1StaticNonStatic obj2 = new Q1StaticNonStatic();
		Q1StaticNonStatic obj3 = new Q1StaticNonStatic();
		//Question1 obj4 = new Question1();
		System.out.println("Static Count :"+obj1.countStatic);
		assertEquals(3,obj1.countStatic);
		//assertEquals(3,obj2.countStatic);
		//assertEquals(3,obj3.countStatic);
	}
	
	@Test
	@DisplayName("Non-Static Variable Counting")
	void instanceCountTest() {
		Q1StaticNonStatic obj1 = new Q1StaticNonStatic();
		Q1StaticNonStatic obj2 = new Q1StaticNonStatic();
		Q1StaticNonStatic obj3 = new Q1StaticNonStatic();
		System.out.println("Non-Static Count :"+obj2.countNonStatic);
		assertEquals(1,obj1.countNonStatic);
		assertEquals(1,obj2.countNonStatic);
		assertEquals(1,obj3.countNonStatic);
	}

}
