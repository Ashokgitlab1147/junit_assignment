package com.innominds.junit;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.Test;

class Q8ListEvenNumbersTest {

	Q8ListEvenNumbers obj = new Q8ListEvenNumbers();
	@Test
	void testListEvenNumbers() {
		ArrayList<Integer> actualResult = obj.listEvenNumbers(10);
		ArrayList<Integer> expectedResult = new ArrayList<>(Arrays.asList(2,4,6,8,10));
		
		assertEquals(expectedResult , actualResult);
	}

}
