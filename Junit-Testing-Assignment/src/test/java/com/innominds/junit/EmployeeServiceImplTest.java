package com.innominds.junit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.powermock.api.mockito.PowerMockito;

import com.innominds.junit.employee.Employee;
import com.innominds.junit.employee.EmployeeDB;
import com.innominds.junit.employee.EmployeeServiceImpl;

class EmployeeServiceImplTest {

	@Test
	void test() {
		EmployeeDB mockEmpDb = PowerMockito.mock(EmployeeDB.class);
		Employee emp = new Employee();
		emp.setId(101);
		emp.setName("Nick");
		emp.setEmail("nick@innominds.com");
		emp.setGender('M');
		emp.setSalary((float) 1_25_320.5);
		PowerMockito.when(mockEmpDb.addEmployee(emp)).thenReturn(true);
		
		EmployeeServiceImpl service = new EmployeeServiceImpl();
		assertTrue(service.addEmployee(emp));
	}
	@Test
	void testShowPayslip() {
		EmployeeDB mockEmpDb = PowerMockito.mock(EmployeeDB.class);
		PowerMockito.when(mockEmpDb.showPaySlip(101)).thenReturn("PaySlip");
		EmployeeServiceImpl service = new EmployeeServiceImpl();
		String payslip = service.showPaySlip(101);
		
		assertEquals("PaySlip",payslip);
		
	}

}
