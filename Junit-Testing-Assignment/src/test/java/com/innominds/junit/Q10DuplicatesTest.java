package com.innominds.junit;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class Q10DuplicatesTest {

	Q10Duplicates obj = new Q10Duplicates();
	@Test
	@DisplayName(">>>Testing method: duplicateCharacters()")
	void testDuplicateCharacters() {
		
		String input = "ashokasAA";
		String actualResult = obj.returnDuplicates(input);
		String expectedResult = "Aas";
		assertEquals(expectedResult , actualResult );
	}
	@Test
	@DisplayName(">>>Testing method: add()")
	void testAdd() {
		int actualResult = obj.result.add(20, 8);
		int expectedResult = 28;
		assertEquals(expectedResult , actualResult);
	}

}
