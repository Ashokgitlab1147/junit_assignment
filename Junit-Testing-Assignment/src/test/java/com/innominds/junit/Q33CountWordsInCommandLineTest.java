package com.innominds.junit;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Q33CountWordsInCommandLineTest {

	@Test
	void testCountWords() {
		Q33CountWordsInCommandLine obj = new Q33CountWordsInCommandLine();
		String s = "as asd asd s";
		int actualResult = obj.countWords(s);
		int expectedResult = 4;

		assertEquals(expectedResult, actualResult);

	}
}
 