
package com.innominds.junit;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.Test;

class Q5ThreadAlivenessTest {

	ThreadAliveness obj = new ThreadAliveness();
	@Test
	void testMaxLivedThread() {
		Q5ThreadAliveness t1 = new Q5ThreadAliveness();
		t1.setName("t1");
		t1.setPriority(2);
		
		Q5ThreadAliveness t2 = new Q5ThreadAliveness();
		t2.setName("t2");
		t2.setPriority(3);
		
		Q5ThreadAliveness t3 = new Q5ThreadAliveness();
		t3.setName("t3");
		t3.setPriority(1);
		
		Q5ThreadAliveness t4 = new Q5ThreadAliveness();
		t4.setName("t4");
		t4.setPriority(8);
		
		Q5ThreadAliveness t5 = new Q5ThreadAliveness();
		t5.setName("t5");
		t5.setPriority(9);
		
		ArrayList<Q5ThreadAliveness> list = new ArrayList<>(Arrays.asList(t1,t2,t3,t4,t5));
		String actualResult = obj.maxLivedThread(list);
		String expectedResult = "t2";
		
		assertEquals(expectedResult , actualResult);
	}
	
	@Test
	void testDemo() {
		Q5ThreadAliveness t1 = new Q5ThreadAliveness();
		t1.setName("t1");
		t1.setPriority(2);
		Q5ThreadAliveness t2 = new Q5ThreadAliveness();
		t2.setName("t2");
		t2.setPriority(3);
		Q5ThreadAliveness t3 = new Q5ThreadAliveness();
		t3.setName("t3");
		t3.setPriority(1);
		Q5ThreadAliveness t4 = new Q5ThreadAliveness();
		t4.setName("t4");
		t4.setPriority(8);
		Q5ThreadAliveness t5 = new Q5ThreadAliveness();
		t5.setName("t5");
		t5.setPriority(9);
		ArrayList<Q5ThreadAliveness> list = new ArrayList<>(Arrays.asList(t1,t2,t3,t4,t5));
		String actualResult = obj.maxLivedThread(list);
		String expectedResult = "t2";
		
		assertEquals(expectedResult , actualResult);
	}
}
