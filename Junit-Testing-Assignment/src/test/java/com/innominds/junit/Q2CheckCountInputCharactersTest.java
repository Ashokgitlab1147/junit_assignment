package com.innominds.junit;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class Q2CheckCountInputCharactersTest {

	private static Q2CheckCountInputCharacters obj;
	@BeforeAll
	static void init() {
		obj = new Q2CheckCountInputCharacters();
	}
	@Test
	@DisplayName("Testing for UpperCase Character")
	void testForUpperCaseCharacter() {
		assertEquals("UpperCase" , obj.checkInputCharacter('A'));
	}
	
	@Test
	@DisplayName("Testing for LowerCase Character")
	void testForLowerCaseCharacter() {
		assertEquals("LowerCase" , obj.checkInputCharacter('a'));
	}
	
	@Test
	@DisplayName("Testing for Digit")
	void testForDigit() {
		assertEquals("Digit" , obj.checkInputCharacter('0'));
	}
	
	@Test
	@DisplayName("Testing for others(special characters)")
	void testForOthers() {
		assertEquals("Other" , obj.checkInputCharacter('@'));
	}
	
	//testing count of upper , lower case , digits and special characters
	@Test
	void testUpperCaseCount() {
		int actualResult = obj.countUpperCase("innominds");
		int expectedResult = 0;
		assertEquals(expectedResult , actualResult);
	}
	@Test
	void testLowerCaseCount() {
		int actualResult = obj.countLowerCase("innominds");
		int expectedResult = 9;
		assertEquals(expectedResult , actualResult);
	}
	@Test
	void testDigitsCount() {
		int actualResult = obj.countDigits("innominds");
		int expectedResult = 0;
		assertEquals(expectedResult , actualResult);
	}
	@Test
	void testOthers() {
		int actualResult = obj.countOthers("innominds");
		int expectedResult = 0;
		assertEquals(expectedResult , actualResult);
	}

}
