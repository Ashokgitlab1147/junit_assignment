package com.innominds.junit;

import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class CalculatorTest {

	private static Calculator calc;
	
	@BeforeAll
	static void beforeAll() {
		calc=new Calculator();
	}
	@AfterAll
	static void afterAll() {
		calc=null;
	}
	@Test
	//@CsvFileSource(resources="/data.csv",numLinesToSkip=1)
	void testAdd() {
		
		int actualResult = calc.add(5,10);
		int expectedResult = 15;
		
		assertEquals(expectedResult , actualResult);
	}
	@Test
	void testMultiplication() {
		int actualResult = calc.mul(10,5);
		int expectedResult = 50;
		
		assertEquals(expectedResult , actualResult);
	}
	
	@Test
	void testConvertToIntException() {
		String str=null;
		assertThrows(IllegalArgumentException.class , () -> calc.convertToInt(str));
		//assertEquals(5,calc.convertToInt("5"));
	}
	@Test
	void testConvertToIntNoException() {
		String str="5";
		//assertThrows(IllegalArgumentException.class , () -> calc.convertToInt(str));
		assertEquals(5,calc.convertToInt(str));
	}
}
