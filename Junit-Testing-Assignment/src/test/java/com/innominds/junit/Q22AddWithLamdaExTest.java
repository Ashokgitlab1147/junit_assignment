package com.innominds.junit;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class Q22AddWithLamdaExTest {
	private static Q22AddWithLamdaEx obj;
	@BeforeAll
	public static void init() {
		obj = new Q22AddWithLamdaEx();
	}
	@Test
	void testAddTwoNumbers() {
		int actualResult = obj.addTwoNumbers(10, 5);
		int expectedResult = 15;
		
		assertEquals(expectedResult , actualResult);
	}

	@Test
	void testAddUsingLambdaEx() {
		int actualResult = obj.addUsingLambdaEx(10, 5);
		int expectedResult = 15;
		
		assertEquals(expectedResult , actualResult);
		
	}
}
