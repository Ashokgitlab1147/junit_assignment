package com.innominds.junit;

public class Calculator {

	public int add(int a , int b) {
		return a+b;
	}
	public int mul(int a , int b) {
		return a*b;
	}
	public int convertToInt(String str) {
		if(str==null || str.trim().length()==0) {
			throw new IllegalArgumentException("String must not null or empty");
		}
		return Integer.valueOf(str);
	}
}
