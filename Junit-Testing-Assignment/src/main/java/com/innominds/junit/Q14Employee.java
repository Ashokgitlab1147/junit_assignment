package com.innominds.junit;

import java.util.List;

public class Q14Employee {
	private int id;
	private String name;
	private String email;
	private char gender;
	private double salary;
	public Q14Employee(int id, String name, String email, char gender, double salary) {
		this.id = id;
		this.name = name;
		this.email = email;
		this.gender = gender;
		this.salary = salary;
	}
	public Q14Employee() {
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public char getGender() {
		return gender;
	}
	public void setGender(char gender) {
		this.gender = gender;
	}
	public double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}
	@Override
	public String toString() {
		return "Employee id=" + id + "\nEmployee name=" + name + "\nEmployee email=" + email + "\nEmployee gender=" + gender + "\nEmployee salary="
				+ salary;
	}

	
	
}
