package com.innominds.junit;
@FunctionalInterface
interface Add {
	int add(int a , int b);
}
public class Q10Duplicates {
	static final int total_chars =256;
	static void fillCharCounts(String s , int[] Count) {
		for(int i=0;i<s.length();i++) {
			Count[s.charAt(i)]++;
		}
	}
	String duplicates="";
	public String returnDuplicates(String str) {
		
		int count[] = new int[total_chars];
		fillCharCounts(str,count);
		for(int i=0 ; i<total_chars;i++) {
			if(count[i]>1) {
				duplicates+=(char)i;
			}
		}
		System.out.println(duplicates);
		return duplicates;
	}
	
	Add result = (x,y)->(x+y); 
}
