package com.innominds.junit;

class DirectionException extends Exception{
	public DirectionException(String name){
		super(name);
	}
}
public class Q11VehiclesDirection {

	public int checkDirection(String vehAdir , String vehBdir) throws DirectionException {
		
		if(vehAdir.equals(vehBdir)) {
			return 1;
		}
		else {
			throw new DirectionException("Vehicles in wrong direction");
		}
	}
	
}
