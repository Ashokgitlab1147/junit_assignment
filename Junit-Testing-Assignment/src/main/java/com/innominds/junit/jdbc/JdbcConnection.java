package com.innominds.junit.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JdbcConnection {

	Connection con=null;
	public boolean createConnection() throws SQLException {
		String url="jdbc:mysql://localhost:3306/world?useSSL=false&serverTimeZone=UTC";
		String user="hbstudent";
		String pass="hbstudent";
		
		try {
			System.out.println("Connecting to the database :"+url);
			con=DriverManager.getConnection(url,user,pass);
			
			
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		if(con!=null && !con.isClosed()) {
			System.out.println("Connection successful!!");
			return true;
		}
		return false;
	}
	public int countTablesInDatabase(String query) throws SQLException {
		Statement st = con.createStatement();
		
		ResultSet rs= st.executeQuery(query);
		rs.next();
		return rs.getInt(1);
	}
	public String checkTable(String query) throws SQLException {
		Statement st = con.createStatement();
		
		ResultSet rs= st.executeQuery(query);
		rs.next();
		String tableName = rs.getString(1);
		return tableName;
	}
	public static void main(String args[]) throws SQLException {
		JdbcConnection obj = new JdbcConnection();
		String query = "SELECT COUNT(*) FROM information_schema.tables as Tables_Count WHERE table_schema = 'world';";
		System.out.println(obj.createConnection());
		System.out.println(obj.countTablesInDatabase(query));
		String q="show tables like \"students\";";
		System.out.println("Table name : "+obj.checkTable(q));
	}
}

