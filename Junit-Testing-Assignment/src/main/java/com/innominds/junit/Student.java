package com.innominds.junit;

public class Student implements Comparable<Student>{

	private String name;
	int marks;
	
	Student(String name , int marks){
		this.name = name;
		this.marks = marks;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getMarks() {
		return marks;
	}

	public void setMarks(int marks) {
		this.marks = marks;
	}
	public static int sort(Student s1 , Student s2) {
		//return s1.marks - s2.marks;
		return s1.name.compareTo(s2.name);
	}

	@Override
	public String toString() {
		return "Student [name=" + name + ", marks=" + marks + "]";
	}

	@Override
	public int compareTo(Student s) {
		//return this.marks-s.marks;
		return this.name.compareTo(s.name);
	}
	public static int compare(Student s1 , Student s2) {
		return s1.name.compareTo(s2.name);
	}
	
}
