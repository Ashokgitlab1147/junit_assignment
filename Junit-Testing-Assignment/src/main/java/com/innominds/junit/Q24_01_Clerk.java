package com.innominds.junit;

public class Q24_01_Clerk extends Q24Employee {

	public Q24_01_Clerk(double basicPay, double hra, double pf, double gratuity, double tax) {
		super(basicPay, hra, pf, gratuity, tax);
	}

	@Override
	double netSalary() {
		return basicPay+hra-pf-gratuity-tax;
	}

	@Override
	boolean displaySalaryInfo() {
		double deductions = (pf+gratuity+tax);
		double total=basicPay+hra;
		String bill= "\nBasic Pay :"+basicPay+
				"\nHRA :"+hra+
				"\n(-)"+
				"\nPF :"+pf+
				"\nGrautity :"+gratuity+
				"\nTax :"+tax+
				"\nTotal :"+total+
				"\nDeductions :"+deductions+
	           "\nTotal Payble Salary :"+this.netSalary();
		if(!bill.isEmpty()) {
			System.out.println("**********Salary of Clerk**********");
			System.out.println(bill);
			return true;
		}
		return false;
	}

}
