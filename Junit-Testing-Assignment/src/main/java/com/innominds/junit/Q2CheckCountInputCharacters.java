package com.innominds.junit;

public class Q2CheckCountInputCharacters {

	public String checkInputCharacter(Character c) {
		if(Character.isUpperCase(c)) {
			return "UpperCase";
		}
		else if(Character.isLowerCase(c)) {
			return "LowerCase";
		}
		else if(Character.isDigit(c)) {
			return "Digit";
		}
		return "Other";
	}
	
	public int countUpperCase(String str) {
		int count=0;
		for(int i=0;i<str.length();i++) {
			if(str.charAt(i)>='A' && str.charAt(i)<='Z') {
				count++;
			}
		}
		return count;
	}
	public int countLowerCase(String str) {
		int count=0;
		for(int i=0;i<str.length();i++) {
			if(str.charAt(i)>='a' && str.charAt(i)<='z') {
				count++;
			}
		}
		return count;
	}
	public int countDigits(String str) {
		int count=0;
		for(int i=0;i<str.length();i++) {
			if(str.charAt(i)>='0' && str.charAt(i)<='9') {
				count++;
			}
		}
		return count;
	}
	public int countOthers(String str) {
		int count=0;
		for(int i=0;i<str.length();i++) {
			if(!(str.charAt(i)>='A' && str.charAt(i)<='Z')&&
					!(str.charAt(i)>='a' && str.charAt(i)<='z')&&
					!(str.charAt(i)>='0' && str.charAt(i)<='9')) {
				count++;
			}
		}
		return count;
	}
}
