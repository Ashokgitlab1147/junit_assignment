package com.innominds.junit;

public abstract class Q24Employee {
	public double basicPay;
	public double hra;
	public double pf;
	public double gratuity;
	public double tax;
	public Q24Employee(double basicPay, double hra, double pf, double gratuity, double tax) {
		this.basicPay = basicPay;
		this.hra = hra;
		this.pf = pf;
		this.gratuity = gratuity;
		this.tax = tax;
	}
	abstract double netSalary();
	abstract boolean displaySalaryInfo();
}
