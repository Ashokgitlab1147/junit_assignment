package com.innominds.junit;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Q8ListEvenNumbers {

	public ArrayList<Integer> listEvenNumbers(int N){
		ArrayList<Integer> list = (ArrayList<Integer>) Stream.iterate(1,x->x+1).limit(N).sorted().collect(Collectors.toList());
		
		ArrayList<Integer> evenNumbers = (ArrayList<Integer>)list.stream().filter(x->x%2==0).sorted().collect(Collectors.toList());

		return evenNumbers;
	}
}
