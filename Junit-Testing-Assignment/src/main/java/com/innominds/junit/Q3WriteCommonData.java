package com.innominds.junit;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;

public class Q3WriteCommonData {

	String[] fileNames = { "one.txt", "two.txt", "three.txt" };

	void writeCommonData(String data) throws IOException {
		BufferedWriter writer = null;
		try {
			for (int i = 0; i < fileNames.length; i++) {
				// System.out.printf("%d\n", i);
				writer = new BufferedWriter(new FileWriter(fileNames[i]));
				writer.write(data);
				writer.close(); // <- flush the BufferedWriter
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			try {
				writer.close();
			}
			catch(Exception e) {}
		}
		System.out.println("Data written successfully!");

	}

	boolean isCommonDataWritten() {
		String[] data = new String[fileNames.length];
		BufferedReader reader = null;
		try {
			for (int i = 0; i < data.length; i++) {
				reader = new BufferedReader(new FileReader(fileNames[i]));
				String s = "";
				while (reader.readLine() != null) {
					s += reader.readLine();
				}
				data[i] = s;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			try {
				reader.close();
			}
			catch(Exception e) {}
			
		}
		//hash set don't store duplicate data
		//so it is useful to check is common data written in all files
		HashSet<String> dataSet = new HashSet<>(Arrays.asList(data));
		return (dataSet.size() == 1);

	}

	public static void main(String[] args) throws IOException {
		Q3WriteCommonData obj = new Q3WriteCommonData();
		String input = "Welcome to Unit Testing training\nWelcome";
		obj.writeCommonData(input);
		if (obj.isCommonDataWritten()) {
			System.out.println("All files have same data");
		}
	}
}