package com.innominds.junit;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class OverflowException extends Exception{
	OverflowException(String name){
		super(name);
	}
}
public class Q25SquaresOfNaturalNumbers {

	public boolean printSquares(int n) throws OverflowException{

		List<Integer> squares = Stream.iterate(1,a->a+1).limit(n).map(x->x*x).collect(Collectors.toList());
		if(squares.size()<=10) {
			System.out.println("Squares of first "+n+" natural numbers");
			for(int i:squares) {
				System.out.print(i+" ,");
			}
			return true;
		}
		else {
			throw new OverflowException("List was overflowed");
		}
	}
	
}
