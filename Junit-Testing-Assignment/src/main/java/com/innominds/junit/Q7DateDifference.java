package com.innominds.junit;

import java.time.LocalDate;
import java.time.Period;

public class Q7DateDifference {

	public String diffBetweenDates(LocalDate date1, LocalDate date2) {
		Period diff = Period.between(date1, date2);
		String s = diff.getYears()+" years,"+diff.getMonths()+" months,"+diff.getDays()+" days";
		System.out.println(s);
		return s;
	}

	/*
	 * public static void main(String args[]) { Q7DateDifference obj = new
	 * Q7DateDifference(); LocalDate date1 = LocalDate.of(2020, 01, 25); LocalDate
	 * date2 = LocalDate.of(2022, 02, 25); String diffBetweenDates =
	 * obj.diffBetweenDates(date1, date2); System.out.println(diffBetweenDates); }
	 */
}
