package com.innominds.junit;

public class Q1StaticNonStatic {

	public static int countStatic = 0;
	public int countNonStatic = 0;
	
	public Q1StaticNonStatic() {
		countStatic++;
		countNonStatic++;
	}
}
