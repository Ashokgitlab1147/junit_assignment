package com.innominds.junit;

import java.util.ArrayList;

public class Q14EmployeeDB {
	ArrayList<Q14Employee> employeeList = new ArrayList<>();
	
	boolean addEmployee(Q14Employee employee) {
		employeeList.add(employee);
		if(!employeeList.contains(employee)) {
			return false;
		}
		return true;
	}
	
	
}
