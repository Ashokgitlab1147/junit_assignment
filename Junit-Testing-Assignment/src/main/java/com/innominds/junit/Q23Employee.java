package com.innominds.junit;

public class Q23Employee implements Comparable<Q23Employee>{

private int id;
	
	private String name;
	
	private String department;

	
	public Q23Employee() {
	}


	public Q23Employee(int id, String name, String department) {
		this.id = id;
		this.name = name;
		this.department = department;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDepartment() {
		return department;
	}


	public void setDepartment(String department) {
		this.department = department;
	}


	@Override
	public String toString() {
		return "Q23Employee [id=" + id + ", name=" + name + ", department=" + department + "]";
	}

	@Override
	public int compareTo(Q23Employee employee) {
		return this.name.compareTo(employee.name);
	}
	
}
