package com.innominds.junit;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Q6EvenNumbers {
	ArrayList<Integer> A1=null;
	ArrayList<Integer> A2=null;
	public ArrayList<Integer> saveEvenNumbers(int N){
		A1 = (ArrayList<Integer>) Stream.iterate(1,x->x+1).limit(N).filter(x->x%2==0).sorted().collect(Collectors.toList());
		return A1;
	}
	
	public ArrayList<Integer> printEvenNumbers(){
		A2 = (ArrayList<Integer>) A1.stream().map(x->x*2).sorted().collect(Collectors.toList());
		return A2;
	}
	
}
