package com.innominds.junit;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Question8 {
	
	public ArrayList<Integer> printEvenNumbers(ArrayList<Integer> list) {
		
		List<Integer> result = list.stream().sorted().filter(x->x%2==0).collect(Collectors.toList());
		result.stream().forEach(System.out::println);
		
		return (ArrayList<Integer>) result;
	}
}
