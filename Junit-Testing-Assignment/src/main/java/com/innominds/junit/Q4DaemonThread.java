package com.innominds.junit;

public class Q4DaemonThread extends Thread{
	public Q4DaemonThread(String name) {
		super(name);
	}
	public void run() {
		if(Thread.currentThread().isDaemon()) {
			System.out.println(getName()+" Daemon Thread");
		}
		else {
			System.out.println(getName()+" User Thread");
		}
	}
}
class Daemon{
	boolean checkDaemonThread(Q4DaemonThread t1){
		t1.start();
		if(t1.isDaemon()) {
			return true;
		}
		return false;
	}
	int daemonThreadException(Q4DaemonThread t1) {
		t1.start();
		boolean alive=false;
		if(t1.isAlive()) {
			t1.setDaemon(true);
			alive=true;
		}
		if(alive) {
			throw new IllegalThreadStateException("Thread already started");
		}
		return 1;
	}
}
