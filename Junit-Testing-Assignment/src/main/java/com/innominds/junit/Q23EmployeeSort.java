package com.innominds.junit;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Q23EmployeeSort {

	public List<String> sortEmployees(List<Q23Employee> employeesList){
		
		List<Q23Employee> sortedList = employeesList.stream().sorted().collect(Collectors.toList());
		ArrayList<String> sortedNames = new ArrayList<>();
		for(Q23Employee emp : sortedList) {
			sortedNames.add(emp.getName());
		}
		return sortedNames;
	}
	
	public static void main(String args[]) {
		List<Student> students =  new ArrayList<>();
		students.add(new Student("Ramananda Sahu" , 80));
		students.add(new Student("Ahhil" , 90));
		students.add(new Student("Balaji" , 92));
		
		students.stream().sorted().forEach(System.out::println);
	}
}
