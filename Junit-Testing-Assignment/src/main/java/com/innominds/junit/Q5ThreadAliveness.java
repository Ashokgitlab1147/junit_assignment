package com.innominds.junit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class Q5ThreadAliveness extends Thread {
	Thread thread;

	public Q5ThreadAliveness() {

		thread = new Thread();

	}

	public void run() {
		if (thread.getPriority() > 7) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}

class ThreadAliveness {

	public String maxLivedThread(ArrayList<Q5ThreadAliveness> threadsLlist) {
		Q5ThreadAliveness t1 = threadsLlist.get(0);
		Q5ThreadAliveness t2 = threadsLlist.get(1);
		Q5ThreadAliveness t3 = threadsLlist.get(2);
		Q5ThreadAliveness t4 = threadsLlist.get(3);
		Q5ThreadAliveness t5 = threadsLlist.get(4);

		Map<String, Integer> map = new HashMap<>();
		t1.start();
		t2.start();
		t3.start();
		t4.start();
		t5.start();
		// highest priority thread will execute after lowest priority thread
		// means highest priority thread will alive maximum
		if (t1.isAlive()) {
			map.put(t1.getName(), t1.getPriority());
		}
		if (t2.isAlive()) {
			map.put(t2.getName(), t2.getPriority());
		}
		if (t3.isAlive()) {
			map.put(t3.getName(), t3.getPriority());
		}
		if (t4.isAlive()) {
			map.put(t3.getName(), t3.getPriority());
		}
		if (t5.isAlive()) {
			map.put(t3.getName(), t3.getPriority());
		}
		int maxValue=Integer.MIN_VALUE;
		String threadName = "";
		for (Entry<String, Integer> entry : map.entrySet()) {
			if(entry.getValue()>maxValue) {
				threadName=entry.getKey();
				maxValue = entry.getValue();
			}
			//System.out.println(entry.getKey() + "," + entry.getValue());
		}
		System.out.println(threadName);
		return threadName;
	}

	public String demo(ArrayList<Q5ThreadAliveness> threadsList) {
		// highest priority thread will execute after lowest priority thread
		// means highest priority thread will alive maximum
		Map<String, Integer> map = new HashMap<>();
		for (int i = 0; i < threadsList.size(); i++) {
			Q5ThreadAliveness thread = threadsList.get(i);
			thread.start();
			if (thread.isAlive()) {
				map.put(thread.getName(), thread.getPriority());
			}
		}
		int maxValue=Integer.MIN_VALUE;
		String threadName = "";
		for (Entry<String, Integer> entry : map.entrySet()) {
			if(entry.getValue()>maxValue) {
				threadName=entry.getKey();
				maxValue = entry.getValue();
			}
			//System.out.println(entry.getKey() + "," + entry.getValue());
		}
		System.out.println(threadName);
		return threadName;
	}
}
