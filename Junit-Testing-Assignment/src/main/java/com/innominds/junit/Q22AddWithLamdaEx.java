package com.innominds.junit;

public class Q22AddWithLamdaEx {

	@FunctionalInterface
	interface Operation {
		public int add(int x, int y);
	}

	public int addTwoNumbers(int x , int y) {
		return x+y;
	}
	//creating lamda expression for adding two numbers
	Operation addition = (int x , int y) -> x+y;
	//using the lamda expression created above in this method
	public int addUsingLambdaEx(int x , int y) {
		return addition.add(x,y);
	}
	
}
