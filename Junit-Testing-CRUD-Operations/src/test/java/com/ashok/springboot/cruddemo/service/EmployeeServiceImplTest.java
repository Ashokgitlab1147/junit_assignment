package com.ashok.springboot.cruddemo.service;

import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.ashok.springboot.cruddemo.dao.EmployeeRepository;
import com.ashok.springboot.cruddemo.entity.Employee;

@SpringBootTest
class EmployeeServiceImplTest {

	@MockBean
	EmployeeRepository employeeRepository;
	@Autowired
	EmployeeService employeeService;

	@Test
	void testGetAllEmployees() {
		List<Employee> employees = new ArrayList<>();
		employees.add(new Employee(100, "Raman", "Sahu", "raman@innominds.com"));
		employees.add(new Employee(101, "Balaji", "Pagadala", "balaji@adjointtech.com"));
		// Mocking statement
		when(employeeRepository.findAll()).thenReturn(employees);
		assertEquals(2, employeeService.findAll().size());
	}

	@Test
	void testFindById() {

		Optional<Employee> optionalEmployee = Optional.ofNullable(new Employee(100, "Raman", "Sahu", "raman@innominds.com"));
		when(employeeRepository.findById(100)).thenReturn(optionalEmployee);
		assertEquals(100,employeeService.findById(100).getId());
	}
	@Test
	void testSaveEmployee() {
		Employee employee = new Employee(100,"Raman" , "Sahu", "raman@innominds.com");
		
		when(employeeRepository.save(employee)).thenReturn(employee);
		
		assertEquals(100 ,employeeRepository.save(employee).getId() );
	}
	@Test
	void testDeleteEmployeeById() {
		Optional<Employee> optionalEmployee = Optional.ofNullable(new Employee(100, "Raman", "Sahu", "raman@innominds.com"));
		when(employeeRepository.findById(100)).thenReturn(optionalEmployee);
		Employee employee = optionalEmployee.get();
	
		employeeService.deleteById(employee.getId());
		verify(employeeRepository).deleteById(employee.getId());
		
	}

}
