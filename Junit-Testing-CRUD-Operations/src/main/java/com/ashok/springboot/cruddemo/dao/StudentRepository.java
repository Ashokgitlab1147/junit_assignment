package com.ashok.springboot.cruddemo.dao;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ashok.springboot.cruddemo.entity.Student;

public interface StudentRepository extends JpaRepository<Student, Serializable> {

}
