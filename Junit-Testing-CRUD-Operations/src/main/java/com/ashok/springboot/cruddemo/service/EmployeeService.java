package com.ashok.springboot.cruddemo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ashok.springboot.cruddemo.dao.EmployeeRepository;
import com.ashok.springboot.cruddemo.entity.Employee;

@Service
public class EmployeeService {

	
	  private EmployeeRepository employeeRepository;
	  
	  @Autowired public EmployeeService(EmployeeRepository theEmployeeRepository) { 
		  employeeRepository = theEmployeeRepository; 
		}
	
	
	public List<Employee> findAll() {
		return employeeRepository.findAll();
	}

	
	public Employee findById(int empId) {
		Optional<Employee> result= employeeRepository.findById(empId);
		Employee theEmployee=null;
		if(result.isPresent()) {
			theEmployee=result.get();
		}
		else {
			throw new RuntimeException("Employee id not found - "+empId);
		}
		return theEmployee;
	}

	
	public void save(Employee employee) {
		employeeRepository.save(employee);
	}

	
	public void deleteById(int id) {
		employeeRepository.deleteById(id);
	}

}
