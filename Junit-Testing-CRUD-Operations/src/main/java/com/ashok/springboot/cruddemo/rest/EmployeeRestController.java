package com.ashok.springboot.cruddemo.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ashok.springboot.cruddemo.entity.Employee;
import com.ashok.springboot.cruddemo.service.EmployeeService;

@RestController
@RequestMapping("/api")
public class EmployeeRestController {

	private EmployeeService employeeService;
	//inject employee dao (it's quick and dirty practice)
	@Autowired
	public EmployeeRestController(EmployeeService theEmployeeService) {
		employeeService = theEmployeeService;
	}
	public EmployeeRestController() {}
	
	//expose or add mapping for GET "/employees" and return list of employess
	@GetMapping("/employees")
	public List<Employee> findAll(){
		return employeeService.findAll();
	}
	//expose or add mapping for GET "/employees/{empId}" to get Employee by id
	@GetMapping("/employees/{empId}")
	public Employee findById(@PathVariable int empId) {
		Employee theEmployee = employeeService.findById(empId);
		if(theEmployee==null) {
			throw new RuntimeException("Employee id not found - "+empId);
		}
		return theEmployee;
	}
	//expose or add mapping for POST "/employees" to save new Employee into DB table
	@PostMapping("/employees")
	public Employee save(@RequestBody Employee employee) {
		employee.setId(0);
		employeeService.save(employee);
		return employee;
	}
	//expose or add mapping for PUT "/employees" to update existing Employee object in DB
	@PutMapping("/employees")
	public Employee updateById(@RequestBody Employee employee) {
		employeeService.save(employee);
		return employee;
	}
	//expose or add mapping for DELETE "/employees" to delete and existing Employee object from DB
	@DeleteMapping("/employees/{empId}")
	public String deleteEmployee(@PathVariable int empId) {
		Employee theEmployee = employeeService.findById(empId);
		if(theEmployee==null) {
			throw new RuntimeException("Employee id not found - "+empId);
		}
		employeeService.deleteById(empId);
		return "Deleted employee id - "+empId;
	}
	
	
	
}
