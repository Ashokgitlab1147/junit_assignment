package com.ashok.springboot.cruddemo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ashok.springboot.cruddemo.dao.StudentRepository;
import com.ashok.springboot.cruddemo.entity.Student;

@Service
public class StudentServiceImpl implements StudentService {

	
	  private StudentRepository studentRepository;
	  
	  @Autowired public StudentServiceImpl(StudentRepository
	  theEmployeeRepository) { studentRepository = theEmployeeRepository; }
	 

	@Override
	public List<Student> findAll() {
		return studentRepository.findAll();
	}

	@Override
	public Student findById(int empId) {
		Optional<Student> result= studentRepository.findById(empId);
		Student theEmployee=null;
		if(result.isPresent()) {
			theEmployee=result.get();
		}
		else {
			throw new RuntimeException("Employee id not found - "+empId);
		}
		return theEmployee;
	}

	@Override
	public void save(Student employee) {
		studentRepository.save(employee);
	}

	@Override
	public void deleteById(int id) {
		studentRepository.deleteById(id);
	}

}
