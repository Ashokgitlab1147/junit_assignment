package com.ashok.springboot.cruddemo.service;

import java.util.List;

import com.ashok.springboot.cruddemo.entity.Employee;
import com.ashok.springboot.cruddemo.entity.Student;

public interface StudentService {
	
	public List<Student> findAll();
	public Student findById(int id);
	public void save(Student employee);
	public void deleteById(int id);
}
