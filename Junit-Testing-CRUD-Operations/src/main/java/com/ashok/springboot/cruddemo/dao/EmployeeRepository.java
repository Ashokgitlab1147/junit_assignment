package com.ashok.springboot.cruddemo.dao;

import java.io.Serializable;
import org.springframework.data.jpa.repository.JpaRepository;
import com.ashok.springboot.cruddemo.entity.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Serializable> {

}
